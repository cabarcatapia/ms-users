FROM python:3.7-alpine3.9
RUN apk update \
    && apk add --update git openssh \
    && pip install --upgrade pip \
    && pip install gunicorn==19.9.0

WORKDIR /app-run
COPY . /app-run

RUN pip install -r /app-run/requirements.txt

ENV GUNICORN_WORKERS 4

ENTRYPOINT ["gunicorn", "-w 4", "--bind", "0.0.0.0:8000", "--access-logfile", "-", "--error-logfile", "-", "server:app", "--reload", "--timeout", "120"]
