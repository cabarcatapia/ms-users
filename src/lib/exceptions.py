from http import HTTPStatus
import json


class UserException(Exception):
    """Base class for other exceptions"""
    def __init__(self, message, status_code):
        super(UserException, self).__init__(message)
        self.status_code = status_code


class UserNotFound(UserException):
    """Raised when a user is not found"""
    def __init__(self):
        super(UserNotFound, self).__init__(
            "User not found", HTTPStatus.NOT_FOUND
        )


class UserExists(UserException):
    """Raised when a user already exists"""
    def __init__(self):
        super(UserExists, self).__init__(
            "User already exists", HTTPStatus.BAD_REQUEST
        )


class Message:

    INFO = 'info'
    ERROR = 'error'

    def __init__(self, message, level):
        self.message = message
        self.level = level

    def to_json(self):
        return json.dumps(self.__dict__)