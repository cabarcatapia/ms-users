from models.db import db
from mongoengine_goodjson import Document


# User model
class User(Document):

    username = db.StringField()
    password = db.StringField()
    name = db.StringField()
    email = db.StringField()
    phone = db.StringField()
    deleted = db.BooleanField(default=False)

    def to_json(self):
        return {
            "username": self.username,
            "name": self.name,
            "email": self.email,
            "phone": self.phone
        }

    meta = {
        'collection': 'users',
        'strict': False
    }
