from service.ServiceImp import ServiceImp
from lib.exceptions import UserException, Message

from flask import request, Response
from bson.json_util import dumps
import json


class ServiceFacade:

	@staticmethod
	def save_user():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.save_user(request.json)
			kwargs['response'] = json.dumps(response)
			kwargs['status'] = 201
		except UserException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def update_user():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.update_user(request.json)
			kwargs['response'] = json.dumps(response)
			kwargs['status'] = 201
		except UserException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def delete_user():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.delete_user(request.args.get('username'))
			kwargs['response'] = json.dumps(response)
			kwargs['status'] = 200
		except UserException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def get_users():
		res = ServiceImp.get_users()
		return Response(
					response=dumps(json.loads(res)),
					status=200, 
					mimetype="application/json"
		)

	@staticmethod
	def get_user(username):

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.get_user(username)
			kwargs['response'] = json.dumps(response.to_json())
			kwargs['status'] = 200
		except UserException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)
