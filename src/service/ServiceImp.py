
from models.User import User
from lib.exceptions import UserNotFound, UserExists
from lib.utils import Utils


class ServiceImp:

	@staticmethod
	def save_user(user):

		try:
			model = User.objects(username=user['username'], deleted=False).first()
		except User.DoesNotExist:
			model = None

		if model is None:
			user_saved = User(**user).save()
		else:
			raise UserExists

		return user_saved.to_json()

	@staticmethod
	def update_user(user):

		model = ServiceImp.get_user(username=user['username'])

		if model:
			updated = False

			# Iterating user keys
			for key in user.keys():

				if user[key] != model[key] and key != 'username':

					model[key] = user[key]
					updated = True

			if updated:
				model.save()

				return 1
			else:
				return 0

	@staticmethod
	def delete_user(username):

		model = ServiceImp.get_user(username)

		if model:
			model.deleted = True
			model.save()

		return 1

	@staticmethod
	def get_users():

		return User.objects(deleted=False).only('username', 'name', 'email', 'phone').to_json()

	@staticmethod
	def get_user(username):

		try:
			model = User.objects(username=username, deleted=False).first()

			if model:
				return model
			else:
				raise UserNotFound
		except User.DoesNotExist:
			raise UserNotFound
